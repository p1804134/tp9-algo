mkfile_dir := $(subst /,\,${CURDIR})

All: main.out

main.out: build/main.o build/Grille.o
	g++ -g build/main.o build/Grille.o -o build/bin/main.out

build/main.o: sources/main.cpp sources/Grille.h
	g++ -g -Wall -c sources/main.cpp -o build/main.o

build/Grille.o: sources/Grille.h sources/Grille.cpp
	g++ -g -Wall -c sources/Grille.cpp -o build/Grille.o

clean:
	-rm build/*.o
	-del /f $(mkfile_dir)\build\*.o

veryclean: clean
	-rm build/bin/main.out
	-del build\bin\main.out
