from PIL import Image
import random
rnd = random.Random()

data = open("colors.txt")

cdata = list(data)
width = int(cdata[0][:-1].split(" ")[0])
height = int(cdata[0][:-1].split(" ")[1])

libs = cdata[1][:-2].split(" ")
for i in range(len(libs)):
    libs[i] = int(libs[i])
#print(libs)
colors = []
c = cdata[2:]
for i in c:
    for j in i[:-1].split(" ")[:-1]:
        colors.append(int(j))

cVals = []
for i in colors:
    cVals.append(( rnd.randint(0,255), rnd.randint(0,255), rnd.randint(0,255) ))

im = Image.new("RGB", (width*10, height*10))
pixels = im.load() # create the pixel map

for i in range(im.size[0]):    # for every col:
    for j in range(im.size[1]):    # For every row
        x = int(i/10)
        y = int(j/10)
        if y*width+x in libs:
            pixels[i,j] = (0, 0, 0) # set the colour accordingly
        else:
            pixels[i,j] = cVals[colors[y*width+x]] # set the colour accordingly

im.show()