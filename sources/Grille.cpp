#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <queue>
#include "Grille.h"

Grille::Grille(const int lignes, const int colonnes)
{
	m_noeuds = std::vector<Noeud> (lignes*colonnes);
	m_L = lignes;
	m_C = colonnes;
}

Grille::Grille(const char *fileName)
{
	m_noeuds = std::vector<Noeud>();
	std::ifstream myfile(fileName);
    if (!myfile.is_open()) throw "file does not exist";
	myfile >> m_C >> m_L;
	int a;
    while (myfile >> a && int(m_noeuds.size())<m_C*m_L) m_noeuds.push_back(a);
	if (int(m_noeuds.size())!=m_C*m_L) throw "file does not respect the correct shape";
}

int Grille::getIndiceGlobal(const int i, const int j)
{
	if (!isValid(i, j)) return -1;
	return i*m_C + j;
}

int Grille::getIIndice(const int indiceGlobal)
{
	if (!isValid(indiceGlobal)) return -1;
	return indiceGlobal/m_C;
}

int Grille::getJIndice(const int indiceGlobal)
{
	if (!isValid(indiceGlobal)) return -1;
	return indiceGlobal%m_C;
}

int Grille::getAltitude(const int i, const int j)
{
	return getAltitude(getIndiceGlobal(i, j));
}

int Grille::getAltitude(const int indiceGlobal)
{
	if (!isValid(indiceGlobal)) throw "index out of bounds";
	return m_noeuds[indiceGlobal]; 
}

void Grille::setAltitude(const int i, const int j, const int altitude)
{
	setAltitude(getIndiceGlobal(i, j), altitude);
}

void Grille::setAltitude(const int indiceGlobal, const int altitude)
{
	if (!isValid(indiceGlobal)) throw "index out of bounds";
	m_noeuds[indiceGlobal] = altitude;
}

float Grille::getDistance(const int indiceGlobalA, const int indiceGlobalB)
{
	int d = getAltitude(indiceGlobalA) - getAltitude(indiceGlobalB);
	return std::sqrt(1 + d*d);
}

float Grille::getDistance(const int iA, const int jA, const int iB, const int jB)
{
	return getDistance(getIndiceGlobal(iA, jA), getIndiceGlobal(iB, jB));
}

int Grille::getVNord(const int i, const int j)
{
	return getIndiceGlobal(i-1, j);
}

int Grille::getVSud(const int i, const int j)
{
	return getIndiceGlobal(i+1, j);
}

int Grille::getVOuest(const int i, const int j)
{
	return getIndiceGlobal(i, j-1);
}

int Grille::getVEst(const int i, const int j)
{
	return getIndiceGlobal(i, j+1);
}

int Grille::getVNord(const int indiceGlobal)
{
	return getVNord(getIIndice(indiceGlobal), getJIndice(indiceGlobal));
}

int Grille::getVSud(const int indiceGlobal)
{
	return getVSud(getIIndice(indiceGlobal), getJIndice(indiceGlobal));
}

int Grille::getVOuest(const int indiceGlobal)
{
	return getVOuest(getIIndice(indiceGlobal), getJIndice(indiceGlobal));
}

int Grille::getVEst(const int indiceGlobal)
{
	return getVEst(getIIndice(indiceGlobal), getJIndice(indiceGlobal));
}

int Grille::getNbL()
{
	return m_L;
}

int Grille::getNbC()
{
	return m_C;
}

bool Grille::isValid(const int indiceGlobal)
{
	return !(indiceGlobal<0 || indiceGlobal>m_L*m_C-1);
}

bool Grille::isValid(const int i, const int j)
{
	return !(i<0 || j<0 || i>=m_L || j>=m_C);
}

std::vector<int> Grille::getColoration(const std::vector<int> &libs)
{
	std::vector<int> coloration(m_C*m_L);
	std::vector<float> couts(m_C*m_L, -2);
	auto cmp = [&couts](int left, int right) { return couts[left] > couts[right]; };
    std::priority_queue<int, std::vector<int>, decltype(cmp)> aTraiter(cmp);

	for (size_t i = 0; i < libs.size(); i++)
	{
		coloration[libs[i]] = i;
		couts[libs[i]] = 0;
		aTraiter.push(libs[i]);
	}

	int next;
	while (!aTraiter.empty())
	{	
		next = aTraiter.top();
		aTraiter.pop();

		std::vector<int> voisins;
		voisins.push_back(getVNord(next));
		voisins.push_back(getVSud(next));
		voisins.push_back(getVEst(next));
		voisins.push_back(getVOuest(next));

		for (auto voisin : voisins)
		{
			if (isValid(voisin))
			{
				if (couts[voisin]!=-1)
				{
					float cout = getDistance(voisin, next) + couts[next];
					if (couts[voisin]==-2 || cout < couts[voisin])
					{
						//if (couts[voisin]==-2) aTraiter.push_back(voisin);
						if (couts[voisin]==-2) 
						{
							couts[voisin] = cout;
							aTraiter.push(voisin);
						}
						else couts[voisin] = cout;
						coloration[voisin] = coloration[next];
					}
				}
			}
		}

		//couts[next] = -1;
	}

	return coloration;
}

void Grille::Affichage()
{
	for (int i = 0; i < m_L; i++)
	{
		std::cout << "|";
		for (int j = 0; j < m_C; j++)
		{
			std::cout << getAltitude(i, j) << "|";
		}
		std::cout << "\n";
	}
}