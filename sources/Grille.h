#ifndef GRAPH_H_INCLUDED
#define GRAPH_H_INCLUDED
#include<vector>

typedef int Noeud;

class Grille
{
public:
    //Ce sont les constructeurs.
    Grille(const int lignes, const int colonnes);
    Grille(const char *fichier);

    //La fonction suivante retourne l'indice global d'un sommet.
    int getIndiceGlobal(const int i, const int j);
    //La fonction suivante retourne l'indice i d'un sommet à partir de l'indice global.
    int getIIndice(const int indiceGlobal);
    //La fonction suivante retourne l'indice j d'un sommet à partir de l'indice global.
    int getJIndice(const int indiceGlobal);

    //La fonction suivante retourne l'altitude d'un sommet.
    int getAltitude(const int i, const int j);
    int getAltitude(const int indiceGlobal);
    //La fonction suivante met à jour l'altitude d'un sommet.
    void setAltitude(const int i, const int j, const int altitude);
    void setAltitude(const int indiceGlobal, const int altitude);

    //La fonction suivante retourne la distance entre deux sommets.
    float getDistance(const int indiceGlobalA, const int indiceGlobalB);
    float getDistance(const int iA, const int jA, const int iB, const int jB);

    //Les fonctions suivantes retournent les indices globaux des voisins d'un sommet. les voisins doivent exister ou une exception sera lancée
    int getVNord(const int i, const int j);
    int getVSud(const int i, const int j);
    int getVOuest(const int i, const int j);
    int getVEst(const int i, const int j);
    int getVNord(const int indiceGlobal);
    int getVSud(const int indiceGlobal);
    int getVOuest(const int indiceGlobal);
    int getVEst(const int indiceGlobal);

    //La fonction suivante affiche les altitudes des sommets.
    void Affichage();

    //La fonction suivante retourne le nombre de lignes de la grille
    int getNbL();
    //La fonction suivante retourne le nombre de colones de la grille
    int getNbC();

    //La fonction suivante retourne vrai si l'indice est valide, faux sinon
    bool isValid(const int indiceGlobal);
    bool isValid(const int i, const int j);

    //La fonction suivante retourne un tableau de coloration qui définit les zones des librairies
    //le tableau en paramètre doit contenir des indices globeaux
    std::vector<int> getColoration(const std::vector<int> &libs);

private:
    std::vector<Noeud> m_noeuds;
    int m_L;
    int m_C;
};

#endif