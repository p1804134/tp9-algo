#include <iostream>
#include <fstream>
#include <random>
#include <cstring>
#include "Grille.h"

using namespace std;

vector<int> loadLibs(const char* fileName)
{
    vector<int> libs;
    ifstream myfile(fileName);
    if (!myfile.is_open()) throw "file does not exist";
    int a;
    while (myfile >> a) libs.push_back(a);
    return libs;
}

void saveColoration(const char* fileName, Grille &g, vector<int> &libs)
{
    vector<int> voronoi = g.getColoration(libs);
    ofstream myfile(fileName);
    myfile << g.getNbC() << " " << g.getNbL() << "\n";
    for (auto i : libs)
    {
        myfile << i << " ";
    }
    myfile << "\n";
    for (int i = 0; i<g.getNbC(); i++)
    {
        for (int j = 0; j<g.getNbL(); j++)
        {
            myfile << voronoi[i*g.getNbC()+j] << " ";
        }
        myfile << "\n";
    }
}

const char *getArg(int argc, char **argv, int dPos, const char* flag, const char* def = nullptr)
{
    if (argc<=1) return def;
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], flag)==0 && i + 1 < argc) return argv[i+1];
    }
    if (def==nullptr) return def;
    return argv[dPos];
}

int main(int argc, char **argv)
{
    int nbRandLibs = 3;

    try { //S'il y a une erreur dans le programme, on la traite ici
        const char* grilleFile = getArg(argc, argv, 1, "-g", "data.txt");
        Grille g(grilleFile);
        //g.Affichage();
        //cout << "\n";

        vector<int> libs;
        if (getArg(argc, argv, 2, "-l")!=nullptr) libs = loadLibs(getArg(argc, argv, 2, "-l"));

        if (getArg(argc, argv, 0, "-R")!=nullptr)
        {
            for (int i=0; i<nbRandLibs; i++)
                libs.push_back(g.getIndiceGlobal(rand()%g.getNbL(), rand()%g.getNbC()));
        }
        
        const char* outputFileColoration = getArg(argc, argv, 3, "-o");
        if (outputFileColoration==nullptr)
        {
            // affiche la coloration
            vector<int> voronoi = g.getColoration(libs);
            for (int i = 0; i < g.getNbL(); i++)
            {
                cout << "|";
                for (int j = 0; j < g.getNbC(); j++)
                {
                    std::cout << voronoi[g.getIndiceGlobal(i, j)] << "|";
                }
                cout << "\n";
            }
        }
        else
            saveColoration(outputFileColoration, g, libs);
    }
    catch (const char* e) {
        cout << "error: " << e;
    }

    return 0;
}
